import java.util.Scanner;

public class GCDRec {
    public static void main(String[]args){
        int a= Integer.parseInt(args[0]);
        int b= Integer.parseInt(args[1]);

        System.out.println(GDCRec(a,b));

    }
    public static int GDCRec(int x,int y){
        if(x==0)
            return y;
        if(y==0)
            return x;
        if(x==y)
            return x;
        if(x>y)
            return GDCRec(x-y,y);

        return GDCRec(x,y-x);
    }
}
