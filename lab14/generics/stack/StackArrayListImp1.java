package generics.stack;

import java.util.ArrayList;
import java.util.List;

public class StackArrayListImp1<T extends Number>implements Stack<T> {
    private ArrayList<T> stack = new ArrayList<T>();
    @Override
    public void push(T item) {
        stack.add(item);
    }

    @Override
    public T pop() {
        if(stack.size()>0)
            return stack.remove(stack.size()-1);
        return null;
    }

    @Override
    public boolean empty() {
        return stack.size()== 0;
    }

    @Override
    public List<T> toList() {
        return stack;
    }



}
