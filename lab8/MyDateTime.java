public class MyDateTime {
    MyDate date;
    MyTime time;

    public MyDateTime(MyDate date, MyTime time) {
        this.date = date;
        this.time=time;
    }

    public String toString(){
        return date + " " + time ;
    }
    public void incrementDay(){
        date.incrementDay();
    }
    public void incrementHour(){
        incrementHour(1);
    }
    public void incrementHour(int diff){
        int dayDiff= time.incrementHour(diff);
        if (dayDiff<0){
            date.decrementDay(-dayDiff);
        }else{
            date.incrementDay(dayDiff);
        }

    }
    public void decrementHour(int diff){
        incrementHour(-diff);
    }
    public void incrementMinute(int diff){
        int dayDiff = time.incrementMinute(diff);

        if (dayDiff<0){
            date.decrementDay(-dayDiff);
        }else{
            date.incrementDay(dayDiff);
        }
    }
    public void decrementMinute(int diff){
        incrementMinute(-diff);
    }

    public void incrementYear(int diff) {
        date.incrementYear(diff);
    }
    public void incrementYear() {
        date.incrementYear();
    }

    public void decrementDay(){
        date.decrementDay();
    }
    public void decrementDay(int diff){
        date.decrementDay(diff);
    }
    public void decrementYear(){
        date.decrementYear();
    }
    public void decrementYear(int diff){
        date.decrementYear(diff);
    }
    public void decrementMonth(){
        date.decrementMonth();
    }

    public void incrementDay(int diff){
        date.incrementDay(diff);
    }
    public void decrementMonth(int diff){
        date.decrementMonth(diff);
    }
    public void incrementMonth(int diff){
        date.incrementMonth(diff);
    }
    public void incrementMonth(){
        date.incrementMonth();
    }
}
