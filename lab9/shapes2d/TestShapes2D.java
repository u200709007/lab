package shapes2d;

public class TestShapes2D {
    public static void main(String[] args) {

        Circle c1 = new Circle(5);
        c1= new Circle(6);
        Circle c2 = new Circle(6);

        System.out.println("c1 == c2: " + (c1==c2));
        System.out.println("c1.equals(c2): "+ c1.equals(c2));

    }
}
