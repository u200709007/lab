package shapes2d;

public class Square {

    protected final int side;

    public Square(int side) {
        this.side = side;
    }

    public int area(){
        return side * side;
    }
    @Override
    public String toString(){
        return "side: " + side;
    }
    @Override
    public boolean equals(Object obj){

        if(this==obj)
            return true;

        if(obj instanceof Square) {
            Square c = (Square) obj;
            return this.side == c.side;
        }
        return false;
    }

}
