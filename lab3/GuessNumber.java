import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) throws IOException {	
		Scanner reader = new Scanner(System.in); //Creates an object to read user input
		Random rand = new Random(); //Creates an object from Random class
        int number =rand.nextInt(100); //generates a number between 0 and 99
        System.out.print("Hi! I'm thinking of a number between 0 and 99.\n"+"Can you guess it: ");
		int guess = reader.nextInt(); //Read the user input
        int attempt=0;

        while(guess!=-1) {
            if(number==guess){
                System.out.println("Congratulations! You won after "+attempt+" attempts.");
                break;
            }else if(number>guess){
                System.out.println("Mine ise greater than your guess.");
                System.out.print("Sorry! \n"+"Type -1 to quit or guess another: ");
                guess = reader.nextInt();            
            }else if(number<guess) {
                System.out.println("Mine ise less than your guess.");
                System.out.print("Sorry! \n"+"Type -1 to quit or guess another:");
                guess = reader.nextInt(); 
            }else{
                System.out.print("Sorry! \n"+"Type -1 to quit or guess another: ");
                guess = reader.nextInt(); 

            }
            attempt++;
            if(guess==-1){
                System.out.println("Sorry, the number was "+ number);
            }
                
        } 
	
		reader.close(); //Close the resource before exiting
	}
		
}
