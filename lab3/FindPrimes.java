public class FindPrimes{
    public static void main(String[]args){
        int x= Integer.parseInt(args[0]);
        for(int a=2;a<=x;a++){
            if(IsPrime(a)== true){
                System.out.print(a+",");
            }
        }
        
    }
    public static boolean IsPrime(int number){
        for(int i=2;i<number;i++) {
            if(number%i == 0){
                return false;
            }
        }
        return true;
    }
}
