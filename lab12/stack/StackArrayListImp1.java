package stack;

import java.util.ArrayList;

public class StackArrayListImp1 implements Stack{
    private ArrayList<Object> stack = new ArrayList<>();
    @Override
    public void push(Object item) {
        stack.add(item);
    }

    @Override
    public Object pop() {
        if(stack.size()>0)
            return stack.remove(stack.size()-1);
        return null;
    }

    @Override
    public boolean empty() {
        return stack.size()== 0;
    }
}
