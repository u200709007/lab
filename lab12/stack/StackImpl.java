package stack;

public class StackImpl implements Stack{
    private StackItem top;

    public StackItem getTop() {
        return top;
    }

    public void setTop(StackItem top) {
        this.top = top;
    }

    @Override
    public void push(Object item) {
        StackItem stackItem = new StackItem(item);
        stackItem.setNext(top);
        top = stackItem;
    }

    @Override
    public Object pop() {
        if (top != null) {
            Object item = top.getItem();
            top = top.getNext();
            return item;
        }
        return null;
    }

    @Override
    public boolean empty() {
        return top == null ;
    }
}
