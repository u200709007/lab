public class Circle {
    int radius;
    Point center;

    public int area(int r){
        return (int)(Math.PI * Math.pow(r,2));
    }
    public int perimeter(int r){
        return (int)(2 * Math.PI * r) ;
    }
    public boolean intersect(Circle circle){
        int distance= (int) Math.sqrt((this.center.xCoord-circle.center.xCoord)*(this.center.xCoord-circle.center.xCoord)+
                (this.center.yCoord-circle.center.yCoord)*(this.center.yCoord-circle.center.yCoord));
        if(distance < this.radius+circle.radius)
            return true;
        else
            return false;
    }



}