public class Main {
    public static void main(String[] args) {
        Point point2= new Point(6,8);
        Point center1= new Point(1,2);
        Point center2= new Point(3,4);

        Rectangle rec = new Rectangle(6, 8, point2);

        System.out.println(rec.area(rec.sideA, rec.sideB));
        System.out.println(rec.perimeter(rec.sideA, rec.sideB));
        System.out.println(rec.corners(point2));

        Circle circle1 = new Circle();
        Circle circle2 = new Circle();

        circle1.radius=10;
        circle2.radius=20;

        circle1.center=center1;
        circle2.center=center2;


        System.out.println(circle1.area(circle1.radius));
        System.out.println(circle1.perimeter(circle1.radius));
        System.out.println(circle1.intersect(circle2));



    }
}