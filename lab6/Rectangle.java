public class Rectangle {
    int sideA;
    int sideB;
    Point topLeft = new Point();

    public Rectangle(int A ,int B,Point point){
        sideA=A;
        sideB=B;
        point=topLeft;
    }
    public int area(int x, int y){
        return x*y;
    }
    public int perimeter(int x,int y){
        return 2*(x+y);
    }
    public String corners(Point point){
        Point a = new Point(point.xCoord,point.yCoord);
        Point b = new Point(point.xCoord+sideB,point.yCoord);
        Point c = new Point(point.xCoord+sideB,point.yCoord-sideA);
        Point d = new Point(point.xCoord,point.yCoord-sideA);

        return "Corner1 ("+a.xCoord+","+a.yCoord+")\n"+"Corner2 ("+b.xCoord+","+b.yCoord+")\n"+
                "Corner3 ("+c.xCoord+","+c.yCoord+")\n"+"Corner4 ("+d.xCoord+","+d.yCoord+")";
    }
}