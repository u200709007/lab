import java.util.Scanner;

public class TicTacToe {
 public static void main(String[] args){
 char[][] board= new char[3][3];
 for(int i =0;i<3;i++){
 for(int j= 0;j<3;j++){
 board[i][j]='-';
 }
 }
 Scanner scanner = new Scanner(System.in);
 System.out.print("Player 1, enter your name: ");
 String p1=scanner.nextLine();
 System.out.print("Player 2, enter your name: ");
 String p2=scanner.nextLine();

 boolean player1=true;

 boolean gameEnded=false;

 while(!gameEnded) {
 drawBoard(board);

 if(player1){
 System.out.println(p1+"'s turn");
 }else{
 System.out.println(p2+"'s turn");
 }

 char ch='-';
 if(player1){
 ch='x';
 }else{
 ch='o';
 }

 int row;
 int col;

 while (true) {
 System.out.print("Please enter row: ");
 row = scanner.nextInt();
 System.out.print("Please enter column: ");
 col = scanner.nextInt();

 if (row > 2 || col > 2) {
 System.out.println("Please enter a valid coordinate.");
 } else if (board[row][col] != '-') {
 System.out.println("This position is made. Try again.");
 } else {
 break;
 }
 }
 board[row][col]=ch;
 player1=!player1;

 if(checkboard(board)=='x'){
 System.out.println(p1+" has won!");
 drawBoard(board);
 gameEnded=true;
 }else if(checkboard(board)=='o'){
 System.out.println(p2+" has won!");
 drawBoard(board);
 gameEnded=true;
 }else{
 if(boardIsFull(board)){
 System.out.println("It is a tie!");
 drawBoard(board);
 gameEnded=true;
 }
 }

 }
 }
 public static void drawBoard(char[][] board){
 for(int i =0;i<3;i++){
 for(int j= 0;j<3;j++){
 System.out.print(board[i][j]);
 }
 System.out.println();
 }
 }
 public static boolean boardIsFull(char[][] board){
 for(int i =0;i<3;i++){
 for(int j= 0;j<3;j++){
 if(board[i][j]=='-'){
 return false;
 }
 }
 }
 return true;
 }
 public static char checkboard(char[][] board){
 for(int i =0;i<3;i++){
 if(board[i][0]==board[i][1]&& board[i][1]==board[i][2]&& board[i][0]!='-'){
 return board[i][0];
 }
 }
 for(int j =0;j<3;j++){
 if(board[0][j]==board[1][j]&& board[1][j]==board[2][j]&& board[0][j]!='-'){
 return board[0][j];
 }
 }
 if(board[0][0]==board[1][1] && board[1][1]==board[2][2]&& board[0][0]!='-'){
 return board[0][0];
 }
 if(board[0][2]==board[1][1] && board[1][1]==board[2][0]&& board[0][2]!='-'){
 return board[0][2];
 }
 return ' ';
 }

}
