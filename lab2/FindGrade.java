public class FindGrade{
    public static void main(String[]args){
        int a = Integer.parseInt(args[0]);
        grade(a);
        
    }
    public static void grade(int a){
        if((a<=100) && (a>=90))
            System.out.println("Your grade is A");
        else if ((a<90) && (a>=80))
            System.out.println("Your grade is B");
        else if ((a<80) && (a>=70))
            System.out.println("Your grade is C");
        else if ((a<70) && (a>=60))
            System.out.println("Your grade is D");
        else if ((a<60) && (a>=0))
            System.out.println("Your grade is F");
        else
            System.out.println("It is not a valid score !");
        
    }
    
}
